Create database db_tk
use db_tk

Create table tbl_alamat(
Id int identity primary key,
alamat varchar(255)
)

Create table tbl_Ortu(
id int identity primary key,
nama varchar(255),
NIK char (10),
tempat_Lahir char(50),
tanggal_Lahir date,
ID_Alamat int Foreign key references tbl_alamat(Id)
)

Create table tbl_login(
id int identity primary key,
password_login varchar(255),
email char(50),
id_ortu int foreign key references tbl_ortu(id),
constraint C_Email_Unik
Unique (email)
)

create table tbl_kelas(
id int identity primary key,
nama_kelas char(50)
)

create table btl_kelas(
id int identity primary key,
nama_kelas char (50)
)

create table tbl_siswa(
id int identity primary key,
nama_Depan char(50),
nama_belakang char(50),
id_ortu int foreign key references tbl_ortu(id),
id_alamat int foreign key references tbl_alamat(id),
id_kelas int foreign key references tbl_kelas(id)
)

create table tbl_dokumen(
ID int identity primary key,
ktp_ortu image,
kartu_keluarga image,
bukti_transfer image,
id_Siswa int foreign key references tbl_siswa(id)
)

create table tr_spp(
id int identity primary key,
spp int,
status char(20),
id_siswa int foreign key references tbl_siswa(id)
)

create table tbl_staff(
id int identity primary key,
nama_staff char(50),
nama_posisi char(50),
salary int
)

create table tbl_kegiatan(
id int identity primary key,
nama_kegiatan char(50),
tanggal_kegiatan date,
waktu_kegiatan time,
id_staff int foreign key references tbl_staff(id),
id_kelas int foreign key references tbl_kelas(id)
)