create database "kindergarten_db"

use "kindergarten_db"

create table tbl_account(
	id int identity primary key,
	email varchar(254) not null, /* Email cannot exceed 254 character. Based on https://stackoverflow.com/a/1199238/10598076 */
	password char(128) not null,
	phone varchar(30) not null,
	isVerified bit not null,
	accountType varchar(10) not null
)

create table tbl_class(
	id int identity primary key,
	className varchar(64),
)

create table tbl_family(
	id int identity primary key,
	accountID int not null,
	classID int,
	
	parent_name varchar(100),
	parent_nik varchar(20), /* NIK kebanyakan 16 karakter. Tapi ada yang karakternya kurang dari 16. Dibuat 20 karakter untuk jaga-jaga */
	parent_birthplace varchar(50), /* 50 character because there's a town called Llanfairpwll-gwyngyllgogerychwyrndrob-wllllantysiliogogogoch. idk, beats me */
	parent_birthdate date,
	
	child_name varchar(100),
	child_birthplace varchar(50),
	child_birthdate date,

	[address] varchar(200),

	constraint fk_family_account foreign key (accountID) references tbl_account(id),
	constraint fk_family_class foreign key (classID) references tbl_class(id),
)

create table tbl_staff(
	id int primary key,
	accountID int not null,
	name varchar(100),
	position varchar(50),

	constraint fk_staff_account foreign key (accountID) references tbl_account(id),
)

create table tbl_document(
	id int identity primary key,
	familyID int not null,
	akta varchar(256), /* all field below is in random string format. the server should redirect the image url to the one on the from the file server*/
	kartukeluarga varchar(256),
	foto varchar(256),
	ktp varchar(256),

	constraint fk_document_family foreign key (familyID) references tbl_family(id),
)

create table tbl_payment(
	id int identity primary key,
	familyID int not null,
	details varchar(256),
	spp_periode char(4), /* Year stored in string like 2018, 2019, 2020 */
	status bit,
	receipt varchar(256), /* field is a file name in random string format */
	
	constraint fk_payment_family foreign key (familyID) references tbl_family(id),
)

create table tbl_evaluation(
	id int identity primary key,
	classID int, 
	familyID int,
	staffID int not null,

	message varchar(2048),
	[date] date,
	
	constraint fk_evaluation_class foreign key (classID) references tbl_class(id),
	constraint fk_evaluation_family foreign key (familyID) references tbl_family(id),
	constraint fk_evaluation_staff foreign key (staffID) references tbl_staff(id),
)

create table tbl_scheduleinfo(
	id int identity primary key,
	staffID int not null,

	[date] date,
	isHoliday bit,
	constraint fk_scheduleinfo_staff foreign key (staffID) references tbl_staff(id),
)

create table tbl_routine_activities(
	id int identity primary key,
	classID int not null,

	[day] int not null,
	isThematic bit not null,
	routineName varchar(24),
	thematicActivityIndex int,

	constraint fk_routine_class foreign key (classID) references tbl_class(id),
)

create table tbl_thematic_activities(
	id int identity primary key,
	classID int not null,

	[date] date not null,
	scheduleIndex int not null,
	[name] varchar(64) not null,

	constraint fk_thematic_class foreign key (classID) references tbl_class(id),
)
