create database db_panda_mandiri

use db_panda_mandiri

create table tbl_akun(
	id int identity primary key,
	email varchar(254) not null, /* Email cannot exceed 254 character. Based on https://stackoverflow.com/a/1199238/10598076 */
	hashPassword char(128) not null,
	terverifikasi bit not null,
	tipeAkun varchar(10) not null
)

CREATE TABLE tbl_wali(
    id int IDENTITY PRIMARY KEY,
    id_akun int FOREIGN KEY REFERENCES tbl_akun(id),
    nama_ayah varchar(100),
    NIK_ayah varchar(16),
    panggilan_ayah varchar(50),
    pendidikan_ayah varchar(50),
    pekerjaan_ayah varchar(20),
    nama_ibu varchar(100),
    NIK_ibu varchar(16),
    panggilan_ibu varchar(50),
    pendidikan_ibu varchar(50),
    pekerjaan_ibu varchar(50),
    No_KTP varchar(20),
    No_HP varchar(15),
    Alamat varchar(254)
);

CREATE TABLE tbl_siswa (
    id int IDENTITY PRIMARY KEY,
    Nama_Lengkap char(100),
    Nama_Panggilan char(20),
    jenis_kelamin char(10),
    id_ortu int FOREIGN KEY REFERENCES tbl_wali(id),
    id_kelas int,
    BirthPlace char(25),
    BirtDate datetime,
    NIK char(20),
    Anak_Ke int,
    Jml_Saudara_Kandung int,
    Golongan_Darah char(5),
    Kelahiran char(10),
    ASI_Berapa_Lama int,
    Sufor_Berapa_Lama int,
    Riwayat_Alergi varchar(254),
    Riwayat_Penyakit varchar(254),
);

create table tbl_dokumen_wali(
	id int identity primary key,
    id_ortu int FOREIGN KEY REFERENCES tbl_wali(id),
	kartukeluarga varchar(256),
	ktp varchar(256),
)
create table tbl_dokumen_siswa(
	id int identity primary key,
    id_siswa int FOREIGN KEY REFERENCES tbl_siswa(id),
	akta varchar(256),
	foto varchar(256),
)